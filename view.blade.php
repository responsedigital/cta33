<!-- Start CTA 33 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A contact section with phone, email, and social icons that includes a map. -->
@endif
<div class="cta-33"  is="fir-cta-33">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="cta-33__wrap">
      <img class="cta-33__map" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594057172/fir/demos/map.jpg' }}">
      <div class="cta-33__contact">
        <h2 class="cta-33__title">{{ $title ?: $faker->text($maxNbChars = 20) }}</h2>
        <p class="cta-33__text">{{ $text ?: $faker->paragraph($nbSentences = 2, $variableNbSentences = true) }}</p>

        <h5 class="cta-33__phone"><a href="tel:{{ $phone_link }}" target="_blank">{{ $phone ?: $faker->phoneNumber }}</a></h5>
        <p class="cta-33__email"><a href="mailto:{{ $email }}" target="_blank">{{ $email  ?: $faker->email }}</a></p>

        @if($facebook || $twitter || $instagram)
          <ul class="cta-33__social">
            @if($facebook)
            <li class="cta-33__social-icon">
              <a href="{{ $facebook['url'] }}" target="_blank">
                <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="CTA_Contact" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="CTA_10_1440" transform="translate(-664.000000, -304.000000)" fill-rule="nonzero">
                            <rect id="Rectangle" fill="#FFFFFF" x="0" y="0" width="1440" height="416"></rect>
                            <g id="social" transform="translate(664.000000, 304.000000)" fill="#333333">
                                <path d="M16,2.66666667 C23.352,2.66666667 29.3333333,8.648 29.3333333,16 C29.3333333,23.352 23.352,29.3333333 16,29.3333333 C8.648,29.3333333 2.66666667,23.352 2.66666667,16 C2.66666667,8.648 8.648,2.66666667 16,2.66666667 Z M16,0 C7.164,0 0,7.164 0,16 C0,24.836 7.164,32 16,32 C24.836,32 32,24.836 32,16 C32,7.164 24.836,0 16,0 Z M13.3333333,13.3333333 L10.6666667,13.3333333 L10.6666667,16 L13.3333333,16 L13.3333333,24 L17.3333333,24 L17.3333333,16 L19.76,16 L20,13.3333333 L17.3333333,13.3333333 L17.3333333,12.2226667 C17.3333333,11.5853333 17.4613333,11.3333333 18.0773333,11.3333333 L20,11.3333333 L20,8 L16.7946667,8 C14.3973333,8 13.3333333,9.056 13.3333333,11.0773333 L13.3333333,13.3333333 Z" id="facebook"></path>
                            </g>
                        </g>
                    </g>
                </svg>
              </a>
            </li>
            @endif
            @if($twitter)
            <li class="cta-33__social-icon">
              <a href="{{ $twitter['url'] }}" target="_blank">
                <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="CTA_Contact" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="CTA_10_1440" transform="translate(-704.000000, -304.000000)" fill-rule="nonzero">
                            <rect id="Rectangle" fill="#FFFFFF" x="0" y="0" width="1440" height="416"></rect>
                            <g id="social" transform="translate(664.000000, 304.000000)" fill="#333333">
                                <path d="M56,2.66666667 C63.352,2.66666667 69.3333333,8.648 69.3333333,16 C69.3333333,23.352 63.352,29.3333333 56,29.3333333 C48.648,29.3333333 42.6666667,23.352 42.6666667,16 C42.6666667,8.648 48.648,2.66666667 56,2.66666667 Z M56,0 C47.164,0 40,7.164 40,16 C40,24.836 47.164,32 56,32 C64.836,32 72,24.836 72,16 C72,7.164 64.836,0 56,0 Z M64.6666667,11.704 C64.0786667,11.9653333 63.4453333,12.1413333 62.7813333,12.2213333 C63.46,11.8146667 63.9786667,11.172 64.2253333,10.4053333 C63.5906667,10.7813333 62.888,11.0546667 62.14,11.2013333 C61.5426667,10.5626667 60.688,10.164 59.7453333,10.164 C57.6253333,10.164 56.068,12.1413333 56.5466667,14.1946667 C53.82,14.0573333 51.4,12.7506667 49.7813333,10.7653333 C48.9213333,12.24 49.336,14.1706667 50.7973333,15.148 C50.26,15.1306667 49.7546667,14.9826667 49.312,14.7373333 C49.276,16.2573333 50.3666667,17.68 51.9453333,17.9973333 C51.484,18.1226667 50.9773333,18.152 50.4626667,18.0533333 C50.88,19.3573333 52.0946667,20.3053333 53.5293333,20.332 C52.1466667,21.4146667 50.4093333,21.8986667 48.6666667,21.6933333 C50.12,22.6253333 51.844,23.168 53.6973333,23.168 C59.7933333,23.168 63.236,18.02 63.028,13.4026667 C63.6706667,12.9413333 64.2266667,12.3626667 64.6666667,11.704 L64.6666667,11.704 Z" id="twitter"></path>
                            </g>
                        </g>
                    </g>
                </svg>
              </a>
            </li>
            @endif
            @if($instagram)
            <li class="cta-33__social-icon">
              <a href="{{ $instagram['url'] }}" target="_blank">
                <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="CTA_Contact" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="CTA_10_1440" transform="translate(-744.000000, -304.000000)" fill-rule="nonzero">
                            <rect id="Rectangle" fill="#FFFFFF" x="0" y="0" width="1440" height="416"></rect>
                            <g id="social" transform="translate(664.000000, 304.000000)" fill="#333333">
                                <path d="M96,2.66666667 C103.352,2.66666667 109.333333,8.648 109.333333,16 C109.333333,23.352 103.352,29.3333333 96,29.3333333 C88.648,29.3333333 82.6666667,23.352 82.6666667,16 C82.6666667,8.648 88.648,2.66666667 96,2.66666667 Z M96,0 C87.164,0 80,7.164 80,16 C80,24.836 87.164,32 96,32 C104.836,32 112,24.836 112,16 C112,7.164 104.836,0 96,0 Z M96,9.44266667 C98.136,9.44266667 98.3893333,9.45066667 99.2333333,9.48933333 C101.402667,9.588 102.413333,10.616 102.513333,12.768 C102.550667,13.612 102.558667,13.864 102.558667,16 C102.558667,18.136 102.550667,18.3893333 102.513333,19.232 C102.413333,21.3826667 101.404,22.4133333 99.2333333,22.512 C98.3893333,22.5493333 98.1373333,22.5586667 96,22.5586667 C93.864,22.5586667 93.6106667,22.5506667 92.768,22.512 C90.5946667,22.412 89.588,21.38 89.488,19.232 C89.4506667,18.3893333 89.4413333,18.136 89.4413333,16 C89.4413333,13.864 89.4506667,13.6106667 89.488,12.768 C89.5866667,10.6146667 90.5973333,9.58666667 92.768,9.488 C93.6106667,9.44933333 93.864,9.44266667 96,9.44266667 Z M96,8 C93.8266667,8 93.556,8.00933333 92.7013333,8.04933333 C89.7946667,8.18266667 88.1813333,9.79466667 88.048,12.7013333 C88.0093333,13.556 88,13.828 88,16 C88,18.1733333 88.0093333,18.4453333 88.048,19.2986667 C88.1813333,22.204 89.7946667,23.8186667 92.7013333,23.952 C93.556,23.9906667 93.8266667,24 96,24 C98.1733333,24 98.4453333,23.9906667 99.3,23.952 C102.201333,23.8186667 103.821333,22.2066667 103.952,19.2986667 C103.990667,18.4453333 104,18.1733333 104,16 C104,13.828 103.990667,13.556 103.952,12.7013333 C103.821333,9.79866667 102.206667,8.18133333 99.3,8.04933333 C98.4453333,8.00933333 98.1733333,8 96,8 L96,8 Z M96,11.892 C93.732,11.892 91.892,13.7306667 91.892,16 C91.892,18.2693333 93.732,20.108 96,20.108 C98.268,20.108 100.108,18.2693333 100.108,16 C100.108,13.732 98.268,11.892 96,11.892 Z M96,18.6666667 C94.5266667,18.6666667 93.3333333,17.4733333 93.3333333,16 C93.3333333,14.528 94.5266667,13.3333333 96,13.3333333 C97.472,13.3333333 98.668,14.5266667 98.668,16 C98.668,17.4733333 97.472,18.6666667 96,18.6666667 Z M100.269333,10.7706667 C99.74,10.7706667 99.3093333,11.2 99.3093333,11.7306667 C99.3093333,12.26 99.7386667,12.6906667 100.269333,12.6906667 C100.8,12.6906667 101.230667,12.2613333 101.230667,11.7306667 C101.230667,11.2 100.801333,10.7706667 100.269333,10.7706667 Z" id="instagram"></path>
                            </g>
                        </g>
                    </g>
                </svg>
              </a>
            </li>
            @endif
          </ul>
        @endif
      </div>
  </div>
</div>
<!-- End CTA 33 -->

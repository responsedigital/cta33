class Cta33 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initCta33()
    }

    initCta33 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: CTA 33")
    }

}

window.customElements.define('fir-cta-33', Cta33, { extends: 'div' })

<?php

namespace Fir\Pinecones\Cta33;

use Roots\Acorn\View\Composer;
use App\Fir;


class Cta33 extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'Cta33.view'
    ];


    /**
     * Data to be passed to view before rendering.
     * Useful for creating values not directly associated to ACF values
     *
     * @return array
     */
    public function with()
    {
        self::injectScripts();
        $data = self::parse($this->data->get('component'));

        /**
         * This is useful for adding anything values you want to auto generate
         * Or data that needs to be pulled and manipulated via external calls
         * Or even just outcomes based on combinations of $data values
         */
        $newData = [

        ];

        return array_merge($data, $newData);
    }

    public function createPhoneLink($component) {
        $phone_link = preg_replace('/\D/', '', $component['phone']);
        return $phone_link;
    }


    /**
     * Parses and returns the component with any data manipulation.
     * Useful for converting options into class names etc
     *
     * @return string
     */
    private function parse($component)
    {
        // $component['test'] = 'This is a test...';
        $component['phone_link'] = self::createPhoneLink($component);
        return $component;
    }


    private static function injectScripts()
    {
        // EXPERIMENTAL ....
        /**
         * https://developer.wordpress.org/reference/functions/wp_enqueue_style/
         *
         * wp_enqueue_style( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, string $media = 'all' )
         *
         * wp_enqueue_style('resp/slider.css', "//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/tiny-slider.css", false, null);
         *
         */

         /**
         * https://developer.wordpress.org/reference/functions/wp_enqueue_script/
         *
         * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false )
         *
         * wp_enqueue_script('resp/slider.js', "//cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/min/tiny-slider.js", false, null, true);
         *
         */
    }
}

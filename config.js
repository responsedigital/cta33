module.exports = {
    'name'  : 'CTA 33',
    'camel' : 'Cta33',
    'slug'  : 'cta-33',
    'dob'   : 'CTA_33_1440',
    'desc'  : 'A contact section with phone, email, and social icons that includes a map.',
}